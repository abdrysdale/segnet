"""A segmentation library for 2D networks"""

# Python imports
from PIL import Image
import logging
import itertools

# Module imports
import numpy as np
from tqdm import tqdm
from shapely import Polygon, MultiPoint, MultiLineString
from centerline.geometry import Centerline
from scipy import ndimage
from scipy.spatial import Voronoi 
from skimage import filters
from skimage.feature import canny

logger = logging.getLogger(__name__)

def _format_numpy(img, channel='sum'):
    """Formats the image to a numpy array

    Args:
        img (str, Pillow image or numpy array) : If a string, will open the image with Pillow.Image.open and then
            convert to a numpy array, if a pillow image, will convert to numpy array or if a numpy array will
            just use the array.
        channel (str or int, optional) : If the image has more than 1 channel, will reduce to 2D image.
            If channel is 'sum', will sum up the first three channels. If channel is 'mean', will take the mean
            of the first 3 channels. Else channel must be an integer of 0, 1 or 2 and will use that axis to return
            the 2D image. Defaults to 'sum'.
    
    Returns:
        np_img (ndarray) : A 2D numpy array of the image.
    """
    # Format to numpy array
    if isinstance(img, str):
        np_img = np.asarray(Image.open(img))
    elif isinstance(img, np.ndarray):
        np_img = img.copy()
    else:
        np_img = np.asarray(img)

    # Formats to correct shape
    if len(np_img.shape) == 2:
        return np_img
    elif len(np_img.shape) != 3:
        logger.critical(f"Image must be 2D or 3D not {len(np_img.shape)}.")
        raise ValueError('Image must be 2D or 3D')
    if np_img.shape[2] == 1:
        return np_img[:, :, 0]
    if np_img.shape[2] != 3:
        logger.error(
            f"3D images should have 1 or 3 channels but got {np_img.shape[2]}. "
            "Will use the first 3 channels and ignore the rest."
        )
        np_img = np_img[:, :, :3]

    if channel == 'sum':
        return np.sum(np_img, axis=2)
    if channel == 'mean':
        return np.mean(np_img, axis=2)
    return np_img[:, :, channel]

def get_img_edges(img, channel='sum', threshold='mean', **kwargs):
    """Returns the edges for an image

    All keyword arguments passed to the canny edge detector.

    Args:
        img (str, Pillow image or numpy array) : If a string, will open the image with Pillow.Image.open and then
            convert to a numpy array, if a pillow image, will convert to numpy array or if a numpy array will
            just use the array.
        channel (str or int, optional) : If the image has more than 1 channel, will reduce to 2D image.
            If channel is 'sum', will sum up the first three channels. If channel is 'mean', will take the mean
            of the first 3 channels. Else channel must be an integer of 0, 1 or 2 and will use that axis to return
            the 2D image. Defaults to 'sum'.
        threshold (float or str) : If threshold is 'mean' will threshold using the mean array value. Else
            will use the threshold value. Defaults to 'mean'.

    Returns:
        edges (ndarray) : Binary array of the image edges.
    """
    # Formats image into 2D numpy array.
    img_np = _format_numpy(img, channel=channel)

    # Thresholds the image
    if threshold == 'mean':
        threshold = np.mean(img_np)
    img_bin = np.zeros(img_np.shape)
    img_bin[img_np > threshold] = 1

    # Gets the edges
    return canny(img_bin, **kwargs)

def get_img_polygon(img, get_edges=True, xsigma=0, ysigma=0, **kwargs):
    """Gets the shapely polygon from the image

    All keyword arguments passed to get_img_edges is get_edges is True or _format_numpy is get_edges is False.

    Args:
        img (ndarray) : If not an numpy ndarray, will load and format the image appropriately.
        get_edges (bool, optional) : If True, will first get the edges from numpy array before extracting the
            polygon. If False, assumes the image is already a binary array containing the edges.

    Returns:
        poly (shapely Polygon) : A shapely Polygon of the edges.
    """
    # Correctly formats the image and extracts the edges if need be
    if get_edges:
        edges = get_img_edges(img, **kwargs)
    else:
        edges = _format_numpy(img, **kwargs)

    return Polygon(trace_edge(edges, xsigma=xsigma, ysigma=ysigma))

def sobel_filter(img):
    """Performs a sobel filter on an object"""
    Kx = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]], np.float32)
    Ky = np.array([[1, 2, 1], [0, 0, 0], [-1, -2, -1]], np.float32)

    Ix = ndimage.filters.convolve(img, Kx)
    Iy = ndimage.filters.convolve(img, Ky)

    G = np.hypot(Ix, Iy)
    G = G / G.max() * 255
    theta = np.arctan2(Iy, Ix)

    return (G, theta)

def trace_edge(edge, max_steps=1e5, xsigma=0, ysigma=0):
    """Traces the edge coordinates to perform an ordered boundary
    
    Args:
        edge (list) : A (nested) list of coordinate pairs for the boundary of the object.
            Expected the format to be (x, y).
        max_steps (int, optional) : The maximum number of points to consider.
            Defaults to 1e5.
        xsigma (float, optional) : The size of the Gaussian kernel to perform smoothing in the x direction.
            Defaults to 0 - no smoothing.
        ysigma (float, optional) : The size of the Gaussian kernel to perform smoothing in the y direction.
            Defaults to 0 - no smoothing.

    Returns:
        coords (list) : An ordered (nested) list of coordinates for the boundary of the object.
    """
    # Gets the coordinates
    x, y = np.where(edge > 0)

    # Gets distance matrix
    mat_x1 = np.zeros((x.size, x.size)) + x[None, :]
    mat_x2 = np.zeros((x.size, x.size)) + x[:, None]
    mat_y1 = np.zeros((y.size, y.size)) + y[None, :]
    mat_y2 = np.zeros((y.size, y.size)) + y[:, None]

    mat_dist = np.sqrt((mat_x1 - mat_x2) ** 2 + (mat_y1 - mat_y2) ** 2)
    dist_max = np.sqrt(edge.shape[0] ** 2 + edge.shape[1] ** 2)
    mat_dist[mat_dist == 0] = 2 * dist_max # Avoids self selecting pixels
    dist0 = mat_dist[0, :]

    # Orders the coordinates
    x0, y0 = x[0], y[0]
    idx = 0
    prev_idxs = [idx]
    coords = [(x0, y0)]
    x1, y1 = -1, -1
    num_steps = 0
    with tqdm(total=x.size, desc="Tracing Edge") as pbar:
        while (x0 != x1 or y0 != y1) and num_steps < max_steps:
            num_steps += 1

            # Gets the position of the nearest pixel
            if num_steps == 1:
                idx = np.where((x0 == x) & (y0 == y))[0][0]
            else:
                idx = np.where((x1 == x) & (y1 == y))[0][0]
            nearest_dist = np.min(mat_dist[idx, :])
            nn_idx = np.where(mat_dist[idx, :] == nearest_dist)[0][0]
            x1, y1 = x[nn_idx], y[nn_idx]
            coords.append((x1, y1))
            prev_idxs.append(nn_idx)

            # Updates the distance matrix to avoid reselection
            mat_dist[:, prev_idxs[-2]] = dist_max + num_steps
            mat_dist[prev_idxs[-2], :] = dist_max + num_steps

            if num_steps == int(x.size / 2):
                prev_idxs.pop(0)
                mat_dist[0, :] = dist0
                mat_dist[:, 0] = dist0
            elif num_steps > int(x.size / 2):
                if np.sqrt((x0 - x1) ** 2 + (y0 - y1) ** 2) < 2:
                    pbar.moveto(x.size)
                    break

            # Updates progress bar
            pbar.update(1)
    coords.pop(-1)
    if xsigma > 0 or ysigma > 0:
        x_coords = np.array(coords, dtype=np.float)[:, 0]
        y_coords = np.array(coords, dtype=np.float)[:, 1]
        smoothed_coords = np.zeros((x_coords.size, 2))
        smoothed_coords[:, 0] = ndimage.gaussian_filter1d(x_coords, sigma=xsigma, mode='wrap')
        smoothed_coords[:, 1] = ndimage.gaussian_filter1d(y_coords, sigma=ysigma, mode='wrap')
        return smoothed_coords.tolist()
    return coords

def get_centerlines(edge_poly, return_coords=False):
    """Gets the centerlines position for the geometry from Voronoi ridges.

    Args:
        edge_poly (shapely polygon) : A shapely polygon of the network boundary.
        return_coords (bool, optional) : If True, will return the coordinates of the centerline ridges
            rather than the shapely MultiLineString object of the centerlines.
            That is (x_coord, y_coord) where x_coord and y_coord are lists.

    Returns:
       centerlines (shapely MultiLineString) : Shapely multi-line string of centerline ridges or ridge vertices 
            coordinates - see return_coords argument.
    """

    # Gets the voronoi ridge vertices that lie within the edge
    vor = Voronoi(edge_poly.exterior.coords)
    ridges = MultiLineString(vor.vertices[vor.ridge_vertices].tolist())
    centerlines = MultiLineString([r for r in ridges.geoms if edge_poly.contains_properly(r)])

    if return_coords:
        # Converts into x and y coordinates
        x_coord = list(itertools.chain(*[[g.xy[0][0], g.xy[0][1]] for g in centerlines.geoms]))
        y_coord = list(itertools.chain(*[[g.xy[1][0], g.xy[1][1]] for g in centerlines.geoms]))
        return (x_coord, y_coord)
    return centerlines

def get_junction_locations(x_coords, y_coords, return_terminals=False):
    coords = np.array(list(zip(x_coords, y_coords)))
    ucoords, cnts = np.unique(coords, return_counts=True, axis=0)
    # A normal line segment has two coordinates. 
    # Thus if a coordinate appears twice it must be the end coordinate of one line and the start coordinate 
    # of another line.
    # If a coordinate appears once, it must be the start or end coordinate of a single line.
    # If a coordinate appears more than twice it must be part of a junction between lines.
    junctions = ucoords[cnts > 2]
    if return_terminals:
        terminals = ucoords[cnts == 1]
        tx, ty = terminals[:, 0].tolist(), terminals[:, 1].tolist()
        jx, jy = junctions[:, 0].tolist(), junctions[:, 1].tolist()
        return (jx, jy), (tx, ty)
    return junctions[:, 0].tolist(), junctions[:, 1].tolist()

