#! /usr/bin/env python
"""An example showing the segmentation of a labelled diagram"""

# Python imports
import os
import sys
from PIL import Image

# Module imports
import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import voronoi_plot_2d

# Local imports
BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)
import segnet

### Extract image edge shape ###
img = np.asarray(Image.open('docs/images/diagram.png'))
plt.subplot(131)
plt.imshow(img, aspect='auto')
plt.title('Image')

edges = segnet.get_img_edges(img, channel='sum', sigma=3)
plt.subplot(132)
plt.imshow(edges, aspect='auto')
plt.title('Edges')

poly = segnet.get_img_polygon(edges, get_edges=False, xsigma=5, ysigma=5)
x, y = poly.exterior.xy
plt.subplot(133)
plt.plot(y, x)
x, y = segnet.get_centerlines(poly, return_coords=True)
plt.scatter(y, x, s=.1)
(jx, jy), (tx, ty) = segnet.get_junction_locations(x, y, return_terminals=True)
plt.scatter(jy, jx, color='green')
plt.scatter(ty, tx, color='red')
plt.gca().invert_yaxis()
plt.title('Centrelines, Junctions and Terminals')

plt.show()
