{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  NIX_SHELL_PRESERVE_PROMPT=1;
  buildInputs = [
    pkgs.poetry
    pkgs.python310Packages.numpy
    pkgs.python310Packages.matplotlib
  ];
  shellHook = ''
    poetry shell
	'';
}
